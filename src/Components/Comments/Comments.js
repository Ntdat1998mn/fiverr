import React, { useState } from "react";

import "../../../node_modules/react-modal-video/scss/modal-video.scss";
import ModalVideo from "react-modal-video";
import Slider from "react-slick";
import test1 from "../../assets/testimonial1.png";
import rooted from "../../assets/rooted-logo-x2.321d79d.png";
import playButton from "../../assets/play-button.png";
import test2 from "../../assets/testimonial2.png";
import naadam from "../../assets/naadam-logo-x2.0a3b198.png";
import test3 from "../../assets/testimonial3.png";
import lavender from "../../assets/lavender-logo-x2.89c5e2e.png";
import test4 from "../../assets/testimonial4.png";
import haer from "../../assets/haerfest-logo-x2.03fa5c5.png";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";

export default function Comments() {
  const [isOpen, setOpen] = useState(false);
  const [linkVideo, setLinkVideo] = useState("");
  const handleOpenModal = (link) => {
    setOpen(true);
    setLinkVideo(link);
  };
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  let item = [
    {
      hinhAnh: test1,
      ten: "Kay Kim, Co-Founder",
      chucVu: rooted,
      quotes: `"It's extremely exciting that Fiverr has freelancers from all over the world — it broadens the talent pool. One of the best things about Fiverr is that while we're sleeping, someone's working."`,
      video:
        "https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/yja2ld5fnolhsixj3xxw",
    },
    {
      hinhAnh: test2,
      ten: "Caitlin Tormey, Chief Commercial Officer",
      chucVu: naadam,
      quotes: `"We've used Fiverr for Shopify web development, graphic design, and backend web development. Working with Fiverr makes my job a little easier every day."`,
      video:
        "https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/plfa6gdjihpdvr10rchl",
    },
    {
      hinhAnh: test3,
      ten: "Brighid Gannon (DNP, PMHNP-BC), Co-Founder",
      chucVu: lavender,
      quotes: `"We used Fiverr for SEO, our logo, website, copy, animated videos — literally everything. It was like working with a human right next to you versus being across the world."`,
      video:
        "https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/rb8jtakrisiz0xtsffwi",
    },
    {
      hinhAnh: test4,
      ten: "Tim and Dan Joo, Co-Founders",
      chucVu: haer,
      quotes: `"When you want to create a business bigger than yourself, you need a lot of help. That's what Fiverr does."`,
      video:
        "https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/bsncmkwya3nectkensun",
    },
  ];
  let renderItem = () => {
    return item.map((item) => {
      return (
        <div>
          <div className="grid grid-cols-2">
            <div className="left relative">
              <img src={item.hinhAnh} alt="" />
              <div className="absolute top-1/2 left-1/2 -translate-y-1/4 -translate-x-1/4">
                <button onClick={() => handleOpenModal(item.video)}>
                  <img className="" src={playButton} alt="" />
                </button>
              </div>
            </div>
            <div className="pl-3 self-center right">
              <div className="flex">
                <h5 className="text-gray-500 text-2xl pr-3 border-r-2">
                  {item.ten}
                </h5>
                <img width={"80px"} src={item.chucVu} alt="" />
              </div>
              <p className="text-3xl mt-5 text-green-700">
                <i>{item.quotes}</i>
              </p>
            </div>
          </div>
        </div>
      );
    });
  };
  let renderItemMobile = () => {
    return item.map((item) => {
      return (
        <div className="grid grid-cols-1">
          <div className="left relative">
            <img src={item.hinhAnh} alt="" />
            <div className="absolute top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2">
              <button onClick={() => handleOpenModal(item.video)}>
                <img className="" src={playButton} alt="" />
              </button>
            </div>
          </div>
        </div>
      );
    });
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: "grey",
          borderRadius: "50%",
        }}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: "grey",
          borderRadius: "50%",
        }}
        onClick={onClick}
      />
    );
  }
  return (
    <div id="comments" className="w-4/5 mx-auto my-10">
      <Desktop>
        <Slider {...settings}>{renderItem()}</Slider>
      </Desktop>
      <Mobile>
        <Slider {...settings}>{renderItemMobile()}</Slider>
      </Mobile>
      <Tablet>
        <Slider {...settings}>{renderItemMobile()}</Slider>
      </Tablet>
      <React.Fragment>
        <ModalVideo
          channel="custom"
          autoplay
          isOpen={isOpen}
          url={linkVideo}
          onClose={() => setOpen(false)}
        />
      </React.Fragment>
    </div>
  );
}
