import React from "react";
import { NavLink } from "react-router-dom";

export default function GroupWorkListItem({ item }) {
  let renderTypeDetail = (data) => {
    return data.map((item) => {
      return <NavLink to={`/categories/${item.id}`}>{item.tenChiTiet}</NavLink>;
    });
  };
  let renderGroupWorkItem = () => {
    return item.dsNhomChiTietLoai.map((item) => {
      return (
        <div className="item">
          <img src={item.hinhAnh} alt="..." />
          <h1 className="font-semibold text-xl mt-4">{item.tenNhom}</h1>
          <p className="font-normal text-lg text-gray-500 mt-4">
            {renderTypeDetail(item.dsChiTietLoai)}
          </p>
        </div>
      );
    });
  };
  return (
    <>
      <h1 className="text-gray-700 text-2xl font-semibold mb-3">
        Explore {item.tenLoaiCongViec}
      </h1>
      <div className="grid gap-3 grid-cols-1 min-[600px]:grid-cols-2 min-[992px]:grid-cols-4">
        {renderGroupWorkItem()}
      </div>
    </>
  );
}
