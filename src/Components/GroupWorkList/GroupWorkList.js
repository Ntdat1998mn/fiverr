import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { workListService } from "../../Service/workListService";
import GroupWorkListItem from "./GoupWorkListItem";
import "./GroupWorkList.css";
import { dataMostPopular } from "../../assets/data";

export default function GroupWorkList() {
  let { groupId } = useParams();
  const [groupWork, setGroupWork] = useState([]);
  useEffect(() => {
    workListService
      .getGroupWork(groupId)
      .then((res) => {
        setGroupWork(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderGroupWorkList = () => {
    return groupWork.map((item) => {
      return <GroupWorkListItem item={item} />;
    });
  };
  let renderMostPopular = () => {
    return dataMostPopular.map((item) => {
      return (
        <div className="flex mostpopular__item p-3 mr-4 min-w-max">
          <img className="w-12 " src={item.img} alt="" />
          <p className="text-lg font-medium ml-2 my-auto">{item.title}</p>
          <div className="w-4 my-auto ml-2">
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="arrow-right"
              class="svg-inline--fa fa-arrow-right"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 448 512"
              data-fa-i2svg=""
            >
              <path
                fill="currentColor"
                d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
              ></path>
            </svg>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="services mt-32 mb-12">
      <div className="w-full mx-auto banner py-12 mb-12 text-center">
        <h1 className="text-white font-bold text-3xl">Graphics & Design</h1>
        <p className="text-white text-2xl font-semibold my-5">
          Design to make you stand out
        </p>
        <button className="text-white text-2xl border border-white p-3 rounded-xl">
          <div className="w-5 inline-block mr-2">
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="far"
              data-icon="circle-play"
              class="svg-inline--fa fa-circle-play"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
              data-fa-i2svg=""
            >
              <path
                fill="currentColor"
                d="M188.3 147.1C195.8 142.8 205.1 142.1 212.5 147.5L356.5 235.5C363.6 239.9 368 247.6 368 256C368 264.4 363.6 272.1 356.5 276.5L212.5 364.5C205.1 369 195.8 369.2 188.3 364.9C180.7 360.7 176 352.7 176 344V167.1C176 159.3 180.7 151.3 188.3 147.1V147.1zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z"
              ></path>
            </svg>
          </div>
          How Fiverr Works
        </button>
      </div>
      <div className="most__popular container mx-auto">
        <h1 class="mb-7 font-semibold text-2xl text-gray-700">
          Most popular in Graphics & Design
        </h1>
        <div className="flex mostpopular__list mb-7 overflow-x-scroll">
          {renderMostPopular()}
        </div>
      </div>
      <div className="container mx-auto mb-12">{renderGroupWorkList()}</div>
      <div class="related-job-title container text-center mx-auto">
        <h1 class="mb-12 font-bold text-2xl text-gray-700">
          Services Related To Graphics &amp; Design
        </h1>
        <div class="tags">
          <span>Minimalist logo design</span>
          <span>Signature logo design</span>
          <span>Mascot logo design</span>
          <span>3d logo design</span>
          <span>Hand drawn logo design</span>
          <span>Vintage logo design</span>
          <span>Remove background</span>
          <span>Photo restoration</span>
          <span>Photo retouching</span>
          <span>Image resize</span>
          <span>Product label design</span>
          <span>Custom twitch overlay</span>
          <span>Custom twitch emotes</span>
          <span>Gaming logo</span>
          <span>Children book illustration</span>
        </div>
      </div>
    </div>
  );
}
