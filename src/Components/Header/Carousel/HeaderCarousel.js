import React, { useState } from "react";
import { Carousel } from "antd";
import banner1 from "../../../assets/1.png";
import banner2 from "../../../assets/2.png";
import banner3 from "../../../assets/3.png";
import banner4 from "../../../assets/4.png";
import banner5 from "../../../assets/5.png";
import fb from "../../../assets/fb.png";
import gg from "../../../assets/google.png";
import netflix from "../../../assets/netflix.png";
import paypal from "../../../assets/paypal.png";
import pg from "../../../assets/pg.png";
const contentStyle = {
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

export default function HeaderCarousel() {
  const banner = [banner1, banner2, banner3, banner4, banner5];
  const partner = [fb, gg, netflix, paypal, pg];
  const [searchInput, setSearchInput] = useState();
  let handleChangeInput = (event) => {
    setSearchInput(event.target.value);
  };

  const renderBanner = () => {
    return banner.map((bannerId) => {
      return (
        <div className="relative">
          <img src={bannerId} alt="" />
          <div className="bannerContent pl-24 absolute top-1/2 left-0 w-1/2 h-1/2">
            <h2 className="text-white">
              Find the perfect <i> freelance </i> services for your business
            </h2>
            <form className="mt-5">
              <input
                onChange={handleChangeInput}
                className="py-3 pl-5 w-4/5 text-base rounded-l h-full"
                type="text"
                placeholder='Try "Social"'
              />
              <a
                href={`/result/${searchInput}`}
                className="text-base py-3 bg-green-700 px-5 text-white font-bold rounded-r h-full"
              >
                Search
              </a>
            </form>
            <h4 className="mt-5 text-lg text-white font-semibold">
              Popular:
              <button>Website Design</button>
              <button>WordPress</button>
              <button>Logo Design</button>
              <button>Video Editing</button>
            </h4>
          </div>
        </div>
      );
    });
  };
  const renderPartner = () => {
    return (
      <div className="partner flex justify-center">
        <p className="self-center">Trusted By: </p>
        <ul className="flex">
          {partner.map((item) => {
            return (
              <li>
                <img className="h-16" src={item} alt="" />
              </li>
            );
          })}
        </ul>
      </div>
    );
  };
  return (
    <div id="banner">
      <Carousel autoplay dots={false}>
        {renderBanner()}
      </Carousel>
      {renderPartner()}
    </div>
  );
}
