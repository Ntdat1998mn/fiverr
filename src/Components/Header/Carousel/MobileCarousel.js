import React, { useState } from "react";
import { Carousel } from "antd";
import banner1 from "../../../assets/1.png";
import banner2 from "../../../assets/2.png";
import banner3 from "../../../assets/3.png";
import banner4 from "../../../assets/4.png";
import banner5 from "../../../assets/5.png";
import fb from "../../../assets/fb.png";
import gg from "../../../assets/google.png";
import netflix from "../../../assets/netflix.png";
import paypal from "../../../assets/paypal.png";
import pg from "../../../assets/pg.png";
const contentStyle = {
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

export default function MobileCarousel() {
  const banner = [banner1, banner2, banner3, banner4, banner5];
  const partner = [fb, gg, netflix, paypal, pg];
  const [searchInput, setSearchInput] = useState();
  let handleChangeInput = (event) => {
    setSearchInput(event.target.value);
  };

  const renderBanner = () => {
    return banner.map((bannerId) => {
      return (
        <div className="relative mobile">
          <img src={bannerId} alt="" />
          <div className="py-14 pl-3 md:pt-20 bannerContent absolute top-0 left-0 w-full h-full">
            <h2 className="text-white">
              Find the perfect <i> freelance </i> services for your business
            </h2>
            <form className="mt-5" action="">
              <input
                onChange={handleChangeInput}
                className="py-3 pl-3 w-3/5 text-base rounded-l h-full"
                type="text"
                placeholder='Try "buiding mobile app"'
              />
              <a
                href={`/result/${searchInput}`}
                className="text-base py-3 bg-green-700 px-5 text-white font-bold rounded-r h-full"
              >
                Search
              </a>
            </form>
          </div>
        </div>
      );
    });
  };
  const renderPartner = () => {
    return (
      <div className="partner flex justify-center">
        <p className="self-center">Trusted By: </p>
        <ul className="flex">
          {partner.map((item) => {
            return (
              <li>
                <img className="h-16" src={item} alt="" />
              </li>
            );
          })}
        </ul>
      </div>
    );
  };
  return (
    <div id="banner">
      <Carousel autoplay dots={false}>
        {renderBanner()}
      </Carousel>
      {renderPartner()}
    </div>
  );
}
