import React from 'react'
import HeaderNavBar from './HeaderNavBar/HeaderNavBar'
import "./Header.css"
import HeaderCarousel from './Carousel/HeaderCarousel'
import {Desktop, Mobile, Tablet} from "../../HOC/Responsive/Responsive"
import MobileCarousel from './Carousel/MobileCarousel'
import MobileNavBar from './HeaderNavBar/MobileNavBar'

export default function Header() {
  return (
    <div id='header'>
      <Desktop>
        <HeaderNavBar></HeaderNavBar>
        <HeaderCarousel></HeaderCarousel>
      </Desktop>
      <Mobile>
        <MobileNavBar></MobileNavBar>
        <MobileCarousel></MobileCarousel>
      </Mobile>
      <Tablet>
        <MobileNavBar></MobileNavBar>
        <MobileCarousel></MobileCarousel>
      </Tablet>
    </div>
  )
}
