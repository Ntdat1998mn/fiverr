import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "../Header.css";
import { userLocalService } from "../../../Service/localService";
import { groupJobService } from "../../../Service/groupJobService";
import { setMenuJob } from "../../../Redux-toolkit/slice/jobSlice";
import { NavLink } from "react-router-dom";

export default function HeaderNavBar() {
  let dispatch = useDispatch();
  const [searchInput, setSearchInput] = useState();
  let handleChangeInput = (event) => {
    setSearchInput(event.target.value);
    console.log(searchInput);
  };
  let user = useSelector((state) => state.userSlice.user);
  let menuJob = useSelector((state) => state.jobSlice.menuJob);
  useEffect(() => {
    groupJobService
      .getMenuCongViec()
      .then((res) => {
        // console.log(res.data.content);
        dispatch(setMenuJob(res.data.content.slice(0, 8)));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
  };
  const renderUser = () => {
    if (user) {
      return (
        <>
          <li>
            <a href="/profile">
              <p className="text-green-500">
                <span className="text-red-500">Hello: </span>
                {user?.user.name}
              </p>
            </a>
          </li>
          <li>
            <a
              onClick={handleLogout}
              className="join hover:bg-green-500 block"
              href="/"
            >
              Log Out
            </a>
          </li>
        </>
      );
    } else {
      return (
        <>
          <li className="">
            <a href="/login">Sign In</a>
          </li>
          <li>
            <a className="join" href="/register">
              Join
            </a>
          </li>
        </>
      );
    }
  };
  const renderMenu = () => {
    return menuJob?.map((loaiCongViec) => {
      return (
        <>
          <li className="relative">
            <a
              className="text-lg text-gray-600"
              href={`/title/${loaiCongViec.id}`}
            >
              {loaiCongViec.tenLoaiCongViec}
            </a>
            <ul className="subItem absolute bg-white shadow-md">
              <li className="py-5 px-10 w-max">
                {loaiCongViec.dsNhomChiTietLoai.map((nhomViec) => {
                  return (
                    <div className="p-2">
                      <p>{nhomViec.tenNhom}</p>
                      {nhomViec.dsChiTietLoai.map((item) => {
                        return (
                          <div className="my-2">
                            <a
                              href={`/categories/${item.id}`}
                              className="relative text-md text-gray-500"
                            >
                              {item.tenChiTiet}
                            </a>
                          </div>
                        );
                      })}
                    </div>
                  );
                })}
              </li>
            </ul>
          </li>
        </>
      );
    });
  };
  const renderNavBar = () => {
    return (
      <div className="py-5 container mx-auto navbar flex justify-between">
        <div className="left flex">
          <div className="logo">
            <a href="/">
              <svg
                className="light"
                width="89"
                height="27"
                viewBox="0 0 89 27"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g fill="#fff">
                  <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
                </g>
                <g fill="#1dbf73">
                  <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
                </g>
              </svg>
              <svg
                className="dark"
                width="89"
                height="27"
                viewBox="0 0 89 27"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g fill="#404145">
                  <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
                </g>
                <g fill="#1dbf73">
                  <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
                </g>
              </svg>
            </a>
          </div>
          <div className="header_search ml-10">
            <form action="" className="flex">
              <input
                onChange={handleChangeInput}
                className="py-2 pl-3 rounded-l"
                type="text"
                placeholder="Find Services"
              />
              <a
                href={`/result/${searchInput}`}
                className="py-2 px-3 rounded-r text-white bg-green-500"
              >
                Search
              </a>
            </form>
          </div>
        </div>
        <div className="right">
          <ul className="flex">
            <li className="">
              <a href="#">Fiverr Business</a>
            </li>
            <li className="">
              <a href="#">Explore</a>
            </li>
            <li className="">
              <a href="#">
                <i className="fa fa-globe"></i> English
              </a>
            </li>
            <li className="">
              <a href="#">US$ USD</a>
            </li>
            <li className="">
              <a href="#">Become a Seller</a>
            </li>
            {renderUser()}
          </ul>
        </div>
      </div>
    );
  };
  return (
    <div id="navBar" className="fixed w-full top-0 left-0 z-10 active">
      {renderNavBar()}
      <div id="subMenu">
        <div className="">
          <ul className="flex justify-around py-2">{renderMenu()}</ul>
        </div>
      </div>
    </div>
  );
}
