import React, { useState } from "react";
import "../../../node_modules/react-modal-video/scss/modal-video.scss";
import ModalVideo from "react-modal-video";
import selling from "../../assets/selling.png";
import playButton from "../../assets/play-button.png";
import video from "../../assets/video1.mp4";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";

export default function Introduction() {
  const [isOpen, setOpen] = useState(false);
  let renderIntro = () => {
    return (
      <div className="grid grid-cols-2 w-4/5 mx-auto pt-14 pb-20">
        <div className="left pr-10">
          <h2 className="text-4xl font-bold mb-5">
            A whole world of <br /> freelance talent at your fingertips
          </h2>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>The best for
              every budget
            </h3>
            <p className="text-gray-600">
              Find high-quality services at every price point. No hourly rates,
              just project-based pricing.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>Quality work
              done quickly
            </h3>
            <p className="text-gray-600">
              Find the right freelancer to begin working on your project within
              minutes.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>Protected
              payments, every time
            </h3>
            <p className="text-gray-600">
              Always know what you'll pay upfront. Your payment isn't released
              until you approve the work.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>24/7 support
            </h3>
            <p className="text-gray-600">
              Questions? Our round-the-clock support team is available to help
              anytime, anywhere.
            </p>
          </div>
        </div>
        <div className="right self-center relative">
          <img src={selling} alt="" />
          <div className="absolute top-1/2 left-1/2 -translate-y-3/4">
            <button onClick={() => setOpen(true)}>
              <img className="" src={playButton} alt="" />
            </button>
          </div>
        </div>
      </div>
    );
  };
  let renderIntroMoblie = () => {
    return (
      <div className="grid grid-cols-1 w-4/5 mx-auto pt-14 pb-20">
        <div className="left pr-10">
          <h2 className="text-3xl font-bold mb-5">
            A whole world of <br /> freelance talent at your fingertips
          </h2>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>The best for
              every budget
            </h3>
            <p className="text-gray-600">
              Find high-quality services at every price point. No hourly rates,
              just project-based pricing.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>Quality work
              done quickly
            </h3>
            <p className="text-gray-600">
              Find the right freelancer to begin working on your project within
              minutes.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>Protected
              payments, every time
            </h3>
            <p className="text-gray-600">
              Always know what you'll pay upfront. Your payment isn't released
              until you approve the work.
            </p>
          </div>
          <div>
            <h3 className="font-semibold text-xl mb-2">
              <i class="fa fa-check-circle text-gray-400 mr-3"></i>24/7 support
            </h3>
            <p className="text-gray-600 mb-2">
              Questions? Our round-the-clock support team is available to help
              anytime, anywhere.
            </p>
          </div>
        </div>
        <div className="right self-center relative">
          <img src={selling} alt="" />
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 ">
            <button onClick={() => setOpen(true)}>
              <img className="" src={playButton} alt="" />
            </button>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div id="introduction" className="" style={{ background: "#f0fdf6" }}>
      <Desktop>{renderIntro()}</Desktop>
      <Mobile>{renderIntroMoblie()}</Mobile>
      <Tablet>{renderIntroMoblie()}</Tablet>
      <React.Fragment>
        <ModalVideo
          channel="custom"
          autoplay
          isOpen={isOpen}
          url={video}
          onClose={() => setOpen(false)}
        />
      </React.Fragment>
    </div>
  );
}
