import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { workListService } from "../../Service/workListService";
import { Rate } from "antd";
import avatar from "../../assets/avatar.jpg";
import { useSelector } from "react-redux";
import { Progress } from "antd";
import { message } from "antd";

export default function JobDetail() {
  let { id } = useParams();
  let user = useSelector((state) => state.userSlice.user);
  const [messageApi, contextHolder] = message.useMessage();
  const success = () => {
    messageApi.open({
      type: "success",
      content: "Thuê công việc thành công!",
    });
  };

  const [workDetail, setWorkDetail] = useState();
  const [comments, setComments] = useState([]);
  const [comment, setComment] = useState();
  const [valueRate, setValueRate] = useState(5);
  const navigate = useNavigate();

  let handleChangeComment = (event) => {
    setComment(event.target.value);
  };
  let handleSubmitComment = (event) => {
    if (user) {
      let commentDay = new Date();
      let year = commentDay.getFullYear();
      let month = commentDay.getMonth() + 1;
      let day = commentDay.getDate();

      let commentModel = {
        id: 0,
        maCongViec: id,
        maNguoiBinhLuan: user.user.id,
        ngayBinhLuan: `${day}/${month}/${year}`,
        noiDung: comment,
        saoBinhLuan: valueRate,
      };
      workListService
        .postComment(commentModel, user.token)
        .then((res) => {
          workListService
            .getComments(id)
            .then((res) => {
              setComments(res.data.content);
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      navigate("/login");
    }
    event.preventDefault();
  };
  let handleThueCongViec = (event) => {
    if (user) {
      let commentDay = new Date();
      let year = commentDay.getFullYear();
      let month = commentDay.getMonth() + 1;
      let day = commentDay.getDate();
      let thueCongViecModel = {
        id: 0,
        maCongViec: id,
        maNguoiThue: user.user.id,
        ngayThue: `${day}/${month}/${year}`,
        hoanThanh: true,
      };

      workListService
        .postThueCongViec(thueCongViecModel, user.token)
        .then((res) => {
          workListService
            .getComments(id)
            .then((res) => {
              success();
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
      event.preventDefault();
    } else {
      navigate("/login");
    }
  };

  useEffect(() => {
    workListService
      .getDetailWork(id)
      .then((res) => {
        setWorkDetail(res.data.content[0]);
      })
      .catch((err) => {
        console.log(err);
      });
    workListService
      .getComments(id)
      .then((res) => {
        setComments(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderComments = () => {
    return comments.map((item, index) => {
      return (
        <>
          <div key={index} className="flex w-full">
            <div className="w-16 h-16 mr-6 flex overflow-hidden rounded-full">
              {item.avatar == "" ? (
                <img className="object-cover" src={avatar} alt="avatar" />
              ) : (
                <img className="object-cover" src={item.avatar} alt="avatar" />
              )}
            </div>
            <div className="w-4/5">
              <div className="flex">
                <h1 className="font-medium">{item.tenNguoiBinhLuan}</h1>
                <div className="my-auto">
                  <svg
                    className="my-auto mx-1"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 1792 1792"
                    width={15}
                    height={15}
                  >
                    <path
                      fill="#ffbe5b"
                      d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                    />
                  </svg>
                </div>
                <h1 className="font-medium text-yellow-500">
                  {item.saoBinhLuan}
                </h1>
              </div>
              <div className="flex my-4">
                <img
                  className="w-5 my-auto mr-2"
                  src="https://fiverr-dev-res.cloudinary.com/general_assets/flags/1f1e8-1f1ed.png"
                  alt=""
                />
                <p className="text-gray-500">Switzerland</p>
              </div>
              <div className="text-gray-500">{item.noiDung}</div>
              <div className="text-gray-500 mt-2">
                Published {item.ngayBinhLuan}
              </div>
              <div className="my-4">
                <button className="text-gray-600 mr-2">Helpful?</button>
                <button className="text-gray-600 mr-2">Like</button>
                <button className="text-gray-600 mr-2">Unlike</button>
              </div>
            </div>
          </div>
          <div className="w-full h-0.5 bg-gray-300 mb-4"></div>
        </>
      );
    });
  };
  if (workDetail) {
    return (
      <div className="mt-52 container mx-auto max-[992px]:px-12">
        <div className="min-[992px]:flex">
          <div className="w-3/5 max-[992px]:w-full">
            <h1 className="text-3xl mb-4 font-bold">
              {workDetail.congViec.tenCongViec}
            </h1>
            <div className="flex">
              <div className="w-10 rounded-full overflow-hidden my-auto">
                <img src={workDetail.avatar} alt="avatar" />
              </div>
              <div className="font-medium capitalize mx-3 my-auto">
                {workDetail.tenNguoiTao}
              </div>
              <div className="text-gray-400 font-medium my-auto">
                Level {workDetail.congViec.saoCongViec} Seller
              </div>
              <div className="bg-gray-500 w-0.5 h-4 my-auto mx-3"></div>
              <div className="h-9 my-auto">
                <Rate value={workDetail.congViec.saoCongViec} />
                <span className="text-yellow-500 mx-1 font-medium">
                  {workDetail.congViec.saoCongViec}
                </span>
                <span className="text-gray-400 font-medium">
                  ({workDetail.congViec.danhGia})
                </span>
              </div>
              <div className="bg-gray-400 w-0.5 h-4 my-auto mx-3"></div>
            </div>
            <div className="text-sm text-gray-500 my-4">2 Order in Queue</div>
            <div className="w-full overflow-hidden">
              <img
                className="w-full hover:scale-110 ease-linear duration-300"
                src={workDetail.congViec.hinhAnh}
                alt=""
              />
            </div>
          </div>
          <div className="w-2/5 min-[992px]:pl-4 max-[992px]:w-full max-[992px]:mt-12">
            <div className="flex">
              <button className="w-1/3 py-3 font-medium border-2 border-r-0 text-gray-500">
                Basic
              </button>
              <button className="w-1/3 py-3 font-medium border-2 border-b-green-500 text-green-500">
                Standard
              </button>
              <button className="w-1/3 py-3 font-medium border-2 border-l-0 text-gray-500">
                Premium
              </button>
            </div>
            <div className="border-2 border-t-0 p-4">
              <div className="flex justify-between font-semibold text-2xl my-4">
                <h1>Basic</h1>
                <h1>US${workDetail.congViec.giaTien}</h1>
              </div>
              <div className="text-gray-500">
                {workDetail.congViec.moTaNgan}
              </div>
              <div className="flex my-4 justify-between">
                <div className="flex text-gray-500">
                  <div className="my-auto fill-gray-500 mr-2">
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm0 14c-3.3 0-6-2.7-6-6s2.7-6 6-6 6 2.7 6 6-2.7 6-6 6z"></path>
                      <path d="M9 4H7v5h5V7H9V4z"></path>
                    </svg>
                  </div>
                  <div className="font-medium">14 Days Delivery</div>
                </div>
                <div className="flex text-gray-500">
                  <div className="my-auto fill-gray-500 mr-2">
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M4.50001 11.4999C6.40001 13.3999 9.60001 13.3999 11.5 11.4999C12.2 10.7999 12.7 9.7999 12.9 8.7999L14.9 9.0999C14.7 10.5999 14 11.8999 13 12.8999C10.3 15.5999 5.90001 15.5999 3.10001 12.8999L0.900012 15.0999L0.200012 8.6999L6.60001 9.3999L4.50001 11.4999Z"></path>
                      <path d="M15.8 7.2999L9.40001 6.5999L11.5 4.4999C9.60001 2.5999 6.40001 2.5999 4.50001 4.4999C3.80001 5.1999 3.30001 6.1999 3.10001 7.1999L1.10001 6.8999C1.30001 5.3999 2.00001 4.0999 3.00001 3.0999C4.40001 1.6999 6.10001 1.0999 7.90001 1.0999C9.70001 1.0999 11.5 1.7999 12.8 3.0999L15 0.899902L15.8 7.2999Z"></path>
                    </svg>
                  </div>
                  <div className="font-medium text-gray-500">
                    Unlimited Revisions
                  </div>
                </div>
              </div>
              <div className="flex">
                <div className="my-auto mr-2">
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 11 9"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#1dbf73"
                  >
                    <path d="M3.645 8.102.158 4.615a.536.536 0 0 1 0-.759l.759-.758c.21-.21.549-.21.758 0l2.35 2.349L9.054.416c.21-.21.55-.21.759 0l.758.758c.21.21.21.55 0 .759L4.403 8.102c-.209.21-.549.21-.758 0Z"></path>
                  </svg>
                </div>
                <div>Good feature</div>
              </div>
              <div className="flex">
                <div className="my-auto mr-2">
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 11 9"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#1dbf73"
                  >
                    <path d="M3.645 8.102.158 4.615a.536.536 0 0 1 0-.759l.759-.758c.21-.21.549-.21.758 0l2.35 2.349L9.054.416c.21-.21.55-.21.759 0l.758.758c.21.21.21.55 0 .759L4.403 8.102c-.209.21-.549.21-.758 0Z"></path>
                  </svg>
                </div>
                <div>Good feature</div>
              </div>
              <div className="flex">
                <div className="my-auto mr-2">
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 11 9"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#1dbf73"
                  >
                    <path d="M3.645 8.102.158 4.615a.536.536 0 0 1 0-.759l.759-.758c.21-.21.549-.21.758 0l2.35 2.349L9.054.416c.21-.21.55-.21.759 0l.758.758c.21.21.21.55 0 .759L4.403 8.102c-.209.21-.549.21-.758 0Z"></path>
                  </svg>
                </div>
                <div>Good feature</div>
              </div>
              {contextHolder}
              <button
                onClick={handleThueCongViec}
                className="font-medium mt-4 bg-green-500 text-white py-2 w-full rounded"
              >
                Continue (US${workDetail.congViec.giaTien})
              </button>
              <div className="text-center mt-2 text-green-500 font-medium">
                Compare Packages
              </div>
            </div>
          </div>
        </div>
        <div className="w-3/5 max-[992px]:w-full">
          <div className="my-10">
            <h1 className="text-xl font-bold mb-5">About This Gig</h1>
            <p className="text-lg text-gray-500">{workDetail.congViec.moTa}</p>
          </div>

          <div className="bg-gray-400 w-full h-0.5"></div>
          <div className="seller my-4">
            <h1 className="text-xl font-bold mb-5">About The Seller</h1>
            <div className="flex">
              <div className="w-24 rounded-full overflow-hidden my-auto">
                <img src={workDetail.avatar} alt="avatar" />
              </div>
              <div className="my-auto ml-4">
                <h1 className="text-gray-500 capitalize font-medium text-2xl">
                  {workDetail.tenNguoiTao}
                </h1>
                <p className="text-gray-500 text-lg">
                  {workDetail.tenChiTietLoai}
                </p>
                <div>
                  <Rate value={workDetail.congViec.saoCongViec} />
                  <span className="text-yellow-500 mx-1 font-medium">
                    {workDetail.congViec.saoCongViec}
                  </span>
                  <span className="text-gray-400 font-medium">
                    ({workDetail.congViec.danhGia})
                  </span>
                </div>
                <button className="bg-white text-gray-500 font-medium mt-4 hover:text-white hover:bg-gray-500 px-5 py-2 border-gray-500 border rounded-lg">
                  Contact Me
                </button>
              </div>
            </div>
          </div>
          <div className="faq my-8">
            <h1 className="font-bold text-2xl">FAQ</h1>
            <p className="text-lg text-gray-500 mt-4">
              There are many passages but the majority?
            </p>
          </div>
          <div className="review my-12">
            <div className=" min-[600px]:flex mb-4">
              <div className="flex">
                <div className="font-bold text-2xl mr-2 my-auto">
                  {workDetail.congViec.danhGia} Reviews
                </div>
                <div className="flex mr-2 my-auto">
                  <Rate value={workDetail.congViec.saoCongViec} />
                  <span className="text-yellow-500 mx-2 font-medium my-auto">
                    {workDetail.congViec.saoCongViec}
                  </span>
                </div>
              </div>
              <div className="flex my-auto">
                <p className="my-auto text-gray-500">Sort By</p>
                <select name="sortDetail" id="sortDetail">
                  <option value="1">Most Recent</option>
                  <option value="2">Most Relevant</option>
                </select>
              </div>
            </div>
            <div className="justify-between min-[600px]:flex">
              <div className="flex flex-wrap min-[600px]:w-1/2">
                <div className="w-full flex font-medium text-blue-500">
                  <div className="text-center my-auto">5 Stars</div>
                  <div className="my-auto w-4/6 px-2 h-6">
                    <Progress
                      strokeColor="#ffb33e"
                      percent={
                        ((workDetail.congViec.danhGia - 20) /
                          workDetail.congViec.danhGia) *
                        100
                      }
                      showInfo={false}
                    />
                  </div>
                  <div className="my-auto">({workDetail.congViec.danhGia})</div>
                </div>
                <div className="w-full flex font-medium text-blue-500">
                  <div className="text-center my-auto">4 Stars</div>
                  <div className="my-auto w-4/6 px-2 h-6">
                    <Progress
                      strokeColor="#ffb33e"
                      percent={(10 / workDetail.congViec.danhGia) * 100}
                      showInfo={false}
                    />
                  </div>
                  <div className="my-auto">(10)</div>
                </div>
                <div className="w-full flex font-medium text-blue-500">
                  <div className="text-center my-auto">3 Stars</div>
                  <div className="my-auto w-4/6 px-2 h-6">
                    <Progress
                      strokeColor="#ffb33e"
                      percent={(5 / workDetail.congViec.danhGia) * 100}
                      showInfo={false}
                    />
                  </div>
                  <div className="my-auto">(5)</div>
                </div>
                <div className="w-full flex font-medium text-blue-500">
                  <div className="text-center my-auto">2 Stars</div>
                  <div className="my-auto w-4/6 px-2 h-6">
                    <Progress
                      strokeColor="#ffb33e"
                      percent={(5 / workDetail.congViec.danhGia) * 100}
                      showInfo={false}
                    />
                  </div>
                  <div className="my-auto">(5)</div>
                </div>
                <div className="w-full flex font-medium text-blue-500">
                  <div className="text-center my-auto">1 Stars</div>
                  <div className="my-auto w-4/6 px-2 h-6">
                    <Progress
                      strokeColor="#ffb33e"
                      percent={0}
                      showInfo={false}
                    />
                  </div>
                  <div className="my-auto">(0)</div>
                </div>
              </div>
              <div className="max-[600px]:mt-5  min-[600px]:w-1/2">
                <h1 className="font-medium">Rating Breakdown</h1>
                <div className="flex">
                  <div className="my-auto">Seller communication level</div>
                  <div className="flex my-auto">
                    <svg
                      className="my-auto mx-1"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 1792 1792"
                      width={15}
                      height={15}
                    >
                      <path
                        fill="#ffbe5b"
                        d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                      />
                    </svg>
                    <div className="text-yellow-500 font-medium">2</div>
                  </div>
                </div>
                <div className="flex">
                  <div>Recommend to a friend</div>
                  <div className="my-auto">
                    <svg
                      className="my-auto mx-1"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 1792 1792"
                      width={15}
                      height={15}
                    >
                      <path
                        fill="#ffbe5b"
                        d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                      />
                    </svg>
                  </div>
                  <div className="text-yellow-500 font-medium">2</div>
                </div>
                <div className="flex">
                  <div>Service as described</div>
                  <div className="my-auto">
                    <svg
                      className="my-auto mx-1"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 1792 1792"
                      width={15}
                      height={15}
                    >
                      <path
                        fill="#ffbe5b"
                        d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                      />
                    </svg>
                  </div>
                  <div className="text-yellow-500 font-medium">2</div>
                </div>
              </div>
            </div>
            <div className="filters my-12 w-full">
              <h1 className="text-2xl font-bold mb-6">Filters</h1>
              <div className="flex">
                <input
                  className="border border-gray-300 p-4 w-4/5 rounded-l"
                  type="text"
                  placeholder="Search reviews"
                />
                <div className="bg-black text-white rounded-r w-1/5 flex">
                  <p className="my-auto w-full text-center">Search</p>
                </div>
              </div>
            </div>
          </div>
          <div className="show__comment mt-12">
            <div className="w-full h-0.5 bg-gray-300 mb-4"></div>
            {renderComments()}
          </div>
          <div className="comment mb-12">
            <div className="flex justify-between mb-12">
              <h1 className="font-bold text-lg my-auto">Leave some comments</h1>
              <div className="flex my-auto h-9">
                <Rate onChange={setValueRate} value={valueRate} />

                <h1 className="font-bold text-lg my-auto ml-2">Rating</h1>
              </div>
            </div>
            <form>
              <textarea
                className="w-full h-24 border-2 border-gray-400 rounded mb-6 p-2"
                name="comment"
                id="comment"
                cols="30"
                rows="10"
                onChange={handleChangeComment}
              ></textarea>
              <button
                onClick={handleSubmitComment}
                className="bg-green-500 text-white py-1 px-3 rounded"
              >
                Comment
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
