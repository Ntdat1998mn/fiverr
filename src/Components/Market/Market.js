import React from "react";
import { useSelector } from "react-redux";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";
import "./Market.css";

export default function Market() {
  const groupJob = useSelector((state) => state.jobSlice.groupJob);
  const renderMarket = () => {
    return (
      <div className="grid grid-cols-6 gap-5 py-5 text-center">
        <div>
          <a href={`/title/${groupJob[0]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[0]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[1]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[1]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[2]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[2]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[3]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[3]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[4]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[4]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[5]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[5]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div></div>
        <div></div>
        <div className="mt-5">
          <a href={`/title/${groupJob[6]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[6]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div className="mt-5">
          <a href={`/title/${groupJob[7]?.id}`}>
            <div className="img pb-4">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[7]?.tenLoaiCongViec}</h3>
          </a>
        </div>
      </div>
    );
  };
  const renderMarketMobile = () => {
    return (
      <div className="grid grid-cols-4 gap-5 py-5 text-center">
        <div>
          <a href={`/title/${groupJob[0]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[0]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[1]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[1]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[2]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[2]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[3]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[3]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[4]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[4]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div>
          <a href={`/title/${groupJob[5]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[5]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div className="">
          <a href={`/title/${groupJob[6]?.id}`}>
            <div className="img">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[6]?.tenLoaiCongViec}</h3>
          </a>
        </div>
        <div className="">
          <a href={`/title/${groupJob[7]?.id}`}>
            <div className="img pb-4">
              <img
                width={"48px"}
                class="main-categories-img mx-auto"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
                alt="Graphics & Design"
                loading="lazy"
              />
            </div>
            <h3 className="pt-5">{groupJob[7]?.tenLoaiCongViec}</h3>
          </a>
        </div>
      </div>
    );
  };
  return (
    <div id="market">
      <div className="w-4/5 mx-auto py-10">
        <h2 className="text-3xl">Explore the marketplace</h2>
        <div className="marketContent py-5">
          <Desktop>{renderMarket()}</Desktop>
          <Tablet>{renderMarket()}</Tablet>
          <Mobile>{renderMarketMobile()}</Mobile>
        </div>
      </div>
    </div>
  );
}
