import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import cr1 from '../../assets/crs1.png'
import cr2 from '../../assets/crs2.png'
import cr3 from '../../assets/crs3.png'
import cr4 from '../../assets/crs4.png'
import cr5 from '../../assets/crs5.png'
import cr6 from '../../assets/crs6.png'
import cr7 from '../../assets/crs7.png'
import cr8 from '../../assets/crs8.png'
import cr9 from '../../assets/crs9.png'
import cr10 from '../../assets/crs10.png' 
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive/Responsive';

export default function Popular() {
    let slick = [cr1,cr2,cr3,cr4,cr5,cr6,cr7,cr8,cr9,cr10];
    let settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 5,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
      };
      let settingsMobile = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
      };
      let settingsTablet = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
      };
      let renderSlick = () => {
        return slick.map((item) => {
            return <div><img width={"200px"} src={item} alt="" /></div>
        })
      }
      function SampleNextArrow(props) {
        const { className, style, onClick } = props;
        return (
          <div
            className={className}
            style={{ ...style, display: "block", background: "grey",borderRadius: "50%"}}
            onClick={onClick}
          />
        );
      }
      
      function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
          <div
            className={className}
            style={{ ...style, display: "block", background: "grey",borderRadius: "50%" }}
            onClick={onClick}
          />
        );
      }
  return (
    <div id='popular' className='w-4/5 mx-auto py-14'>
      <Desktop>
      <h2 className='text-4xl mb-5'>Popular professional services</h2>
        <Slider {...settings}>
            {renderSlick()}
        </Slider>
      </Desktop>
      <Mobile>
        <h2 className='text-3xl mb-5'>Popular professional services</h2>
          <Slider {...settingsMobile}>
            {renderSlick()}
          </Slider>
      </Mobile>
      <Tablet>
          <h2 className='text-3xl mb-5'>Popular professional services</h2>
          <Slider {...settingsTablet}>
            {renderSlick()}
          </Slider>
      </Tablet>
    </div>
  )
}
