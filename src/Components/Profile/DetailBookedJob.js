import { message } from 'antd';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { groupJobService } from '../../Service/groupJobService';
import { userService } from '../../Service/userService';

export default function () {
    let user = useSelector((state) => state.userSlice.user);
    let [bookedJob,setBookedJob] = useState([])
    useEffect(() => {
        userService.getCongViec(user.token).then((res) => {
                console.log(res.data.content);
                setBookedJob(res.data.content)
              })
              .catch((err) => {
               console.log(err);
              });
    },[])
    let handleDeleteJob = (id) => {
        groupJobService.xoaCongViecDaThue(id,user.token).then((res) => {
                console.log(res);
                message.success("Xoá Thành Công!")
                let index = bookedJob.findIndex((job) => job.id == id);
                bookedJob.splice(index,1)
                setBookedJob([...bookedJob])
              })
              .catch((err) => {
               console.log(err);
              });
    }
    let renderbookedJob = () => {
        return bookedJob.map((job) => {
            return(
                <div className='job grid grid-cols-3 p-5 m-5'>
                    <div className=''>
                        <img src={job.congViec.hinhAnh} alt="" />
                    </div>
                    <div className='ml-4 col-span-2'>
                        <h2 className='font-semibold text-xl'>{job.congViec.tenCongViec}</h2>
                        <p className='text-gray-500'>{job.congViec.moTaNgan}</p>
                        <div className='flex justify-between'>
                            <p><span className='text-yellow-400'><i class="fa fa-star"></i> {job.congViec.saoCongViec}</span><span className='text-gray-400 ml-4'>({job.congViec.danhGia})</span></p>
                            <p className='text-lg'>${job.congViec.giaTien}</p>
                        </div>
                        <div className='text-right mt-3'>
                            <a href={`/jobDetail/${job.congViec.id}`} className='bg-green-500 text-white font-semibold py-3 px-2 rounded-md mr-4'>View Detail</a>
                            <button onClick={() => handleDeleteJob(job.id)} className='bg-red-500 text-white font-semibold py-3 px-2 rounded-md'>Delete</button>
                        </div>
                    </div>
                </div>
            )
        })
    }
  return (
    <div id='bookedJob'>
        <div className='bookedJob'>
            <h2 className='text-center my-5 font-bold text-2xl text-green-500'>Your Booked Jobs:</h2>
            {renderbookedJob()} 
        </div>
    </div>
  )
}
