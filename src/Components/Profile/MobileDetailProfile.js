import { message } from 'antd';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setOpen } from '../../Redux-toolkit/slice/userSlice';
import { userService } from '../../Service/userService';
import ModalUpdate from './ModalUpdate';

export default function MobileDetailProfile() {
  let dispatch = useDispatch();
    let user = useSelector((state) => state.userSlice.user)
    const showModal = () => {
      dispatch(setOpen(true))
    };
    console.log("user: ",user);
    const uploadAvatar = (e) => {
        console.log("e: ",e.target.files);
        userService.uploadAvatar(e.target.files[0],user.token).then((res) => {
                console.log(res);
              })
              .catch((err) => {
               message.error("Tính năng đang được xây dựng T.T")
              });
    }
    const handleShowDetail = () => {
        document.getElementById("descProfile").classList.toggle("hidden");
    }
    const renderAvatar = () => {
        if(user.user.avatar.length == 0){
            return(
                <div className='bg-gray-300 w-full h-full flex items-center justify-center relative'>
                        <div className='bg-gray-300 icon absolute w-full h-full text-center self-center opacity-0 hover:opacity-100'>
                            <input onChange={(e) => uploadAvatar(e)} type="file" className='opacity-0 cursor-pointer w-full h-full absolute z-10 top-0 left-0'/>
                            <i class="fa fa-camera-retro text-white text-5xl top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 absolute -z-0"></i>
                        </div>
                        <p className='text-white text-2xl font-bold'>{user?.user.name}</p>
                    </div>
            )
        }else{
            return(
                <div className='bg-gray-300 w-full h-full flex items-center justify-center relative'>
                        <div className='icon absolute w-full h-full text-center self-center opacity-0 hover:opacity-100'>
                            <input onChange={(e) => uploadAvatar(e)} type="file" className='opacity-0 cursor-pointer w-full h-full absolute z-10 top-0 left-0'/>
                            <i class="fa fa-camera-retro text-white text-5xl top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 absolute -z-0"></i>
                        </div>
                        <img className='w-full h-full object-cover' src={user.user.avatar} alt="" />
                    </div>
            )
        }
    }
    const renderDetailProfile = () => {
        return(
            <>
            <div className='avatarUser p-5 m-5'>
                <div className='avatar mx-auto'>
                    {renderAvatar()}
                </div>
                <div className='user mx-auto text-center my-4 font-semibold text-xl'>
                    <h2>{user.user.email}</h2>
                    <button onClick={showModal}><i class="fa fa-pencil-alt text-gray-500 hover:text-black mt-3"></i></button>
                </div>
            </div>
            <div className='text-center'>
                <button onClick={handleShowDetail} id='buttonDetail' className='text-green-500 text-lg py-2 px-3 rounded-xl'>Show More Detail <i class="fa fa-arrow-down"></i></button>
            </div>
        <div id='descProfile' className='desc p-5 m-5 hidden'>
            <div className='description'>
                <div className='flex justify-between'>
                    <h2 className='font-semibold text-lg'>Description</h2>
                    <button onClick={showModal} className=''><i class="fa fa-pencil-alt text-gray-500 hover:text-black mt-3"></i></button>
                </div>
                <div className='text-gray-500'>
                    <div className='flex justify-between mt-2 mb-4'>
                        <p className=''>Name:</p>
                        <p className=''>{user.user.name}</p>
                    </div>
                    <div className='flex justify-between mt-2 mb-4'>
                        <p className=''>Phone:</p>
                        <p className=''>{user.user.phone}</p>
                    </div>
                    <div className='flex justify-between mt-2 mb-4'>
                        <p className=''>Birthday: </p>
                        <p className=''>{user.user.birthday}</p>
                    </div>
                </div>
            </div>
            <div className='skills my-5'>
                <div className='flex justify-between'>
                    <h2 className='font-semibold text-lg'>Skills</h2>
                    <button onClick={showModal} className=''><i class="fa fa-pencil-alt text-gray-500 hover:text-black mt-3"></i></button>
                </div>
                <div>
                    {user?.user.skill.map((skill) => {
                        return <p className='mt-2 mb-4'>{skill}</p>
                    })}
                </div>
            </div>
            <div className='certification my-5'>
                <div className='flex justify-between'>
                    <h2 className='font-semibold text-lg'>Certification</h2>
                    <button onClick={showModal} className=''><i class="fa fa-pencil-alt text-gray-500 hover:text-black mt-3"></i></button>
                </div>
                <div>
                    {user.user.certification.map((certification) => {
                        return <p className='mt-2 mb-4'>{certification}</p>
                    })}
                </div>
            </div>
            <div className='social my-5'>
                <h2 className='font-semibold text-lg hover:underline'>Linked Accounts</h2>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-facebook mr-3"></i>Facebook</a></div>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-google mr-3"></i>Google</a></div>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-github mr-3"></i>Github</a></div>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-twitter-square mr-3"></i>Twitter</a></div>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-dribbble mr-3"></i>Dirbble</a></div>
                <div><a className='text-blue-500  hover:underline' href="#"><i class="fab fa-stack-overflow mr-3"></i>Stack Overflow</a></div>
            </div>
        </div>
            </>
        )
    }
  return (
    <div className='detail'>
        {renderDetailProfile()}
        <ModalUpdate></ModalUpdate>
    </div>
  )
}
