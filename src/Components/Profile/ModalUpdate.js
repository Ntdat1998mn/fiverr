import React, { useEffect } from 'react'
import { Button, DatePicker, Form, Input, message, Modal, Radio, Select } from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setOpen, setUser } from '../../Redux-toolkit/slice/userSlice';
import { validDate, validLetterAndSpace, validNumber } from '../../Pages/RegisterPage/validate';
import { userService } from '../../Service/userService';
import { userLocalService } from '../../Service/localService';

export default function ModalUpdate() {
  let dispatch = useDispatch();
  let user = useSelector((state) => state.userSlice.user);
  let [cloneUser,setCloneUser] = useState({...user});
  let open = useSelector((state) => state.userSlice.open);
  const handleOk = () => {
    let {name,phone,birthday} = cloneUser.user;
    let regex = validDate(cloneUser.user.birthday);
    if(regex.valid){
      if(name.length !== 0 && phone.length !== 0 && birthday.length !== 0){
        document.getElementById("result").innerHTML = "Success!";
        userService.getUpdate(cloneUser.user,user.user.id)
              .then((res) => {
                console.log(res);
                message.success("Update success!")
                userLocalService.setUser(cloneUser);
                dispatch(setUser(cloneUser))
              })
              .catch((err) => {
               console.log(err);
              });
        setTimeout(() => {
          dispatch(setOpen(false));
        }, 3000);
      }else{
        document.getElementById("result").innerHTML = "Cannot be Emty!"
      }
    }
  };
  const handleCancel = () => {
    dispatch(setOpen(false));
  };
  const handleSetForm = (e) => {
    if(e.target.name == "name"){
      let result = validLetterAndSpace(e.target.value);
      if(result.valid){
          setCloneUser({...cloneUser,user:{...cloneUser.user,name:e.target.value}});
      }
      document.getElementById(`${e.target.name}`).innerHTML = result.message ;
    }else if( e.target.name == "phone"){
      let result = validNumber(e.target.value);
      if(result.valid){
        setCloneUser({...cloneUser,user:{...cloneUser.user,phone:e.target.value}});
      }
      document.getElementById(`${e.target.name}`).innerHTML = result.message ;
    }else if( e.target.name == "birthday"){
      let result = validDate(e.target.value);
      setCloneUser({...cloneUser,user:{...cloneUser.user,birthday:e.target.value}});
      document.getElementById(`${e.target.name}`).innerHTML = result.message ;
    }else if( e.target.name == "gender"){
      setCloneUser({...cloneUser,user:{...cloneUser.user,gender:e.target.value}});
      console.log(cloneUser);
    }
  }
  let handleSetSelectForm = (e) => {
    let array = [...cloneUser.user.certification];
    let index = array.findIndex((cert) => cert == e)
    if(index == -1){
      array.push(e);
      setCloneUser({...cloneUser,user:{...cloneUser.user,certification:[...array]}})
      console.log(cloneUser);
    }
  }
  let handleSetSkillForm = (e) => {
    let array = [...cloneUser.user.skill];
    
    let index = array.findIndex((cert) => cert == e)
    if(index == -1){
      array.push(e);
      setCloneUser({...cloneUser,user:{...cloneUser.user,skill:[...array]}})
      console.log(cloneUser);
    }
  }
  const renderForm = () => {
    return(
      <div>
      <div className='grid grid-cols-2 mx-3 gap-2'>
        <Form.Item label="Email">
          <Input value={user.user.email} disabled/>
        </Form.Item>
        <Form.Item label="Phone">
          <Input value={cloneUser.user.phone} name="phone" onChange={(e) => handleSetForm(e)}/>
          <p className='text-red-500' id='phone'></p>
        </Form.Item>
        <Form.Item label="Name">
          <Input name='name' onChange={(e) => handleSetForm(e)} value={cloneUser.user.name}/>
          <p className='text-red-500' id='name'></p>
        </Form.Item>
        <Form.Item label="Birthday">
          <Input name='birthday' onChange={(e) => handleSetForm(e)} value={cloneUser.user.birthday}/>
          <p className='text-red-500' id='birthday'></p>
        </Form.Item>
      </div>
      <div>
          <Form.Item label="Gender">
            <Radio.Group onChange={(e) => handleSetForm(e)} name='gender' value={cloneUser.user.gender}>
              <Radio  value={true}> Male </Radio>
              <Radio value={false}> Female </Radio>
            </Radio.Group>
            <p className='text-red-500' id='gender'></p>
          </Form.Item>
      </div>
      <div className='grid grid-cols-1 lg:grid-cols-2 mx-3 gap-2'>
        <Form.Item style={{width:"100%"}} label="Certification">
          <Select onChange={(e) => handleSetSelectForm(e)}>
            <Select.Option value="Cyber security certification">Cyber security certification</Select.Option>
            <Select.Option value="Digital certification">Digital certification</Select.Option>
            <Select.Option value="Environmental certification">Environmental certification</Select.Option>
            <Select.Option value="Food safety certification">Food safety certification</Select.Option>
            <Select.Option value="Music recording sales certification">Music recording sales certification</Select.Option>
            <Select.Option value="Professional certification (computer technology)">Professional certification (computer technology)</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item style={{width:"100%"}} label="Skills">
          <Select onChange={(e) => handleSetSkillForm(e)}>
            <Select.Option value="Design">Design</Select.Option>
            <Select.Option value="Marketing">Marketing</Select.Option>
            <Select.Option value="Writing">Writing</Select.Option>
            <Select.Option value="Programing">Programing</Select.Option>
            <Select.Option value="Signing">Signing</Select.Option>
            <Select.Option value="Professional certification (computer technology)">Events</Select.Option>
          </Select>
        </Form.Item>
        <p id='result' className='py-3 text-red-500'></p>
      </div>
      </div>
    )
  }
  return (
    <div id='modalUpdate'>
      <Modal
        open={open}
        title={<p className='text-center text-2xl'>Update User</p>}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <button className='bg-red-500 text-white text-md font-semibold py-3 px-4 rounded-xl mr-3 hover:shadow-2xl' key="back" onClick={handleCancel} >Cancel</button>,
          <button className='bg-green-500 text-white text-md font-semibold py-3 px-4 rounded-xl mr-3 hover:shadow-2xl' type="primary" onClick={handleOk}>Save</button>
        ]}
      >
        {renderForm()}
      </Modal>
    </div>
  )
}
