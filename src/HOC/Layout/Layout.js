import React from 'react'
import Footer from '../../Components/Footer/Footer'
import HeaderNavBar from '../../Components/Header/HeaderNavBar/HeaderNavBar'
import MobileNavBar from '../../Components/Header/HeaderNavBar/MobileNavBar'
import {Desktop, Mobile, Tablet} from "../../HOC/Responsive/Responsive"

export default function Layout({Component}) {
  return (
    <div>
      <Desktop>
        <HeaderNavBar></HeaderNavBar>
        <div className='mt-20'>
        <Component></Component>
        </div>
        <Footer></Footer>
      </Desktop>
      <Mobile>
        <MobileNavBar></MobileNavBar>
        <div className='mt-20'>
        <Component></Component>
        </div>
        <Footer></Footer>
      </Mobile>
      <Tablet>
        <MobileNavBar></MobileNavBar>
        <div className='mt-20'>
        <Component></Component>
        </div>
        <Footer></Footer>
      </Tablet>
    </div>
  )
}
