import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import signIn from "../../assets/signIn.jpg";
import { userService } from "../../Service/userService";
import { useDispatch, useSelector } from "react-redux";
import { setUser } from "../../Redux-toolkit/slice/userSlice";
import { userLocalService } from "../../Service/localService";
import { useNavigate } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";

export default function LoginPage() {
  const dispatch = useDispatch();
  let user = useSelector((state) => state.userSlice?.user);
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    let model = {
      email: values.username,
      password: values.password,
    };
    userService
      .getLogIn(model)
      .then((res) => {
        dispatch(setUser(res.data.content));
        userLocalService.setUser(res.data.content);
        message.success("Login Success!");
      })
      .catch((err) => {
        console.log(err);
        document.getElementById("loginErr").innerHTML =
          "Email or Password is Invalid. Check Again!";
      });
  };
  const setNavigate = () => {
    setTimeout(() => {
      navigate("/");
    }, 2000);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const renderLogin = () => {
    if (user) {
      return (
        <>
          <h2 className="pb-10 text-center text-3xl font-bold text-green-500">
            Sign In to Fiverr
          </h2>
          <h4 className="text-red-500 font-bold text-3xl text-center">
            You Have Already Logged In!
          </h4>
          {setNavigate()}
        </>
      );
    } else {
      return (
        <>
          <h2 className="pb-10 text-center text-3xl font-bold text-green-500">
            Sign In to Fiverr
          </h2>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Your Email"
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input placeholder="Input Your Email" />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password placeholder="Input Your Password" />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <div>
                <p id="loginErr" className="mb-3 text-red-500"></p>
                <button className="bg-green-500 text-white py-3 px-5 text-lg font-semibold rounded-3xl hover:bg-green-700 hover:animate-bounce">
                  LOGIN
                </button>
                <a className="text-blue-500 ml-5 underline " href="/register">
                  Register Now?
                </a>
              </div>
            </Form.Item>
          </Form>
        </>
      );
    }
  };
  return (
    <div id="login">
      <Desktop>
        <div className="grid grid-cols-2 w-3/5 mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg">
          <div>
            <img src={signIn} alt="" />
          </div>
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Desktop>
      <Mobile>
        <div className="grid grid-cols-1 w-full mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg overflow-hidden">
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Mobile>
      <Tablet>
        <div className="grid grid-cols-1 w-full mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg overflow-hidden">
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Tablet>
    </div>
  );
}
