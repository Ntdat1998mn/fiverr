import React from 'react'
import signup from '../../assets/signup.jpg'
import {
 Checkbox,DatePicker,Form,Input,
  Radio} from 'antd';
import { useState } from 'react';
import { validEmail, validLetterAndSpace, validNumber, validPassword, validDate } from './validate';
import { userService } from '../../Service/userService';
import { useNavigate } from 'react-router-dom';
import {Desktop, Mobile, Tablet} from "../../HOC/Responsive/Responsive"

export default function RegisterPage() {
    let navigate = useNavigate();
    let [userForm,setUserForm] = useState({
        name: "",
        email: "",
        password: "",
        rePassword: "",
        phone: "",
        birthday:"",
        gender:true,
        agree:false,
    })
    let [valid,setValid] = useState({
        name: false,
        email: false,
        password: false,
        phone: false,
        birthday: false
    }) 
    const handleSetForm = (e) => {
        // console.log("e: ",e.target);
        // console.log("userForm: ",userForm);
        if(e.target.name == "name"){
            let result = validLetterAndSpace(e.target.value);
            // console.log("result: ",result);
            if(result.valid){
                setUserForm({...userForm,name:e.target.value});
                valid[e.target.name] = true;
            }
            document.getElementById(`${e.target.name}`).innerHTML = result.message ;
        }else if( e.target.name == "email"){
            let result = validEmail(e.target.value);
            // console.log("result: ",result);
            if(result.valid){
                setUserForm({...userForm,email:e.target.value});
                valid[e.target.name] = true;
            }else{
                valid[e.target.name] = false;
            }
            document.getElementById(`${e.target.name}`).innerHTML = result.message ;
        }else if( e.target.name == "password"){
            let result = validPassword(e.target.value);
            // console.log("result: ",result);
            if(result.valid){
                setUserForm({...userForm,password:e.target.value})
            }
            document.getElementById(`${e.target.name}`).innerHTML = result.message ;
        }else if( e.target.name == "rePassword"){
            let result = validPassword(e.target.value);
            // console.log("result: ",result);
            if(result.valid){
                setUserForm({...userForm,rePassword:e.target.value})
            }
            document.getElementById(`${e.target.name}`).innerHTML = result.message ;
        }else if( e.target.name == "phone"){
            let result = validNumber(e.target.value);
            // console.log("result: ",result);
            if(result.valid){
                setUserForm({...userForm,phone:e.target.value});
                valid[e.target.name] = true;
            }
            document.getElementById(`${e.target.name}`).innerHTML = result.message ;
        }else if(e.target.name == "gender"){
            setUserForm({...userForm,gender:e.target.value});
            document.getElementById(`${e.target.name}`).innerHTML = "" ;
        }else if(e.target.name == "agree"){
            setUserForm({...userForm,agree:e.target.checked});
            document.getElementById('agree').innerHTML = "";
        }
        
    }
    const handleSetDate = (date,dateString) => {
        let result = validDate(dateString);
        if(result.valid){
            setUserForm({...userForm,birthday:dateString});
            setValid({...valid,birthday:true})
        }else{
            setValid({...valid,birthday:false})
        }
        document.getElementById(`birthday`).innerHTML = result.message ;
      };
    const handleRegister = () => {
        let isNotEmty = true;
        // kiem tra rong
        for (let key in userForm){
            if(userForm[key].length == 0){
               document.getElementById(`${key}`).innerHTML = "Cannot Be Emty!";
               isNotEmty = false;
            }
        }
        if(isNotEmty){
            // kiem tra password:
            if(userForm.password == userForm.rePassword){
                document.getElementById(`password`).innerHTML = "";
                document.getElementById(`rePassword`).innerHTML = "";
                valid.password = true;
                // kiem tra dong y dieu khoan:
                if(userForm.agree){
                    let allValid = true;
                    console.log("valid: ",valid);
                    console.log("userForm: ",userForm);
                    for(let key in valid){
                       allValid &= valid[key];
                    }
                    if(allValid){
                        userService.getSignUp(userForm).then((res) => {
                                console.log(res);
                                document.getElementById("result").innerHTML = `Create account success!`;
                                setTimeout(() => {
                                    navigate("/login");
                                },3000)
                              })
                              .catch((err) => {
                               console.log(err);
                               document.getElementById("result").innerHTML =
                               `Email already exists!`
                              });
                    }
                }else{
                    document.getElementById('agree').innerHTML = "You must agree Terms of Servie";
                }
            }else{
                document.getElementById(`password`).innerHTML = "Password must be same!";
                document.getElementById(`rePassword`).innerHTML = "Password must be same!";
                valid.password = false;
            }
        }
    }
    const renderRegister = () => {
        return(
            <div>
                <h2 className='text-center text-green-500 font-semibold text-3xl pb-5'>REGISTER</h2>
                <form action="">
                    <Form.Item
                     label={<i class="fa fa-user mr-2"></i>}>
                        <Input placeholder='Your Name' value={userForm.name} name="name" onChange={(e) => handleSetForm(e)} />
                        <p className='text-red-500' id='name'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-envelope mr-2"></i>}>
                        <Input onChange={(e) => handleSetForm(e)} placeholder='Your Email' name='email' />
                        <p className='text-red-500' id='email'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-lock mr-2"></i>}>
                        <Input.Password onChange={(e) => handleSetForm(e)} placeholder='Your Password' name='password' />
                        <p className='text-red-500' id='password'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-key mr-2"></i>}>
                        <Input.Password onChange={(e) => handleSetForm(e)} placeholder='Repeat Your Password' name='rePassword' />
                        <p className='text-red-500' id='rePassword'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-phone mr-2"></i>}>
                        <Input value={userForm.phone} onChange={(e) => handleSetForm(e)} placeholder='Your Phone' name='phone' />
                        <p className='text-red-500' id='phone'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-birthday-cake mr-2"></i>}>
                        <DatePicker onChange={handleSetDate} placeholder='yyyy-mm-dd' name='birthday' />
                        <p className='text-red-500' id='birthday'></p>
                    </Form.Item>
                    <Form.Item label={<i class="fa fa-venus-mars mr-2"></i>}>
                        <Radio.Group name='gender' value={userForm.gender} onChange={(e) => handleSetForm(e)}>
                            <Radio  value={true}> Male </Radio>
                            <Radio value={false}> Female </Radio>
                        </Radio.Group>
                        <p className='text-red-500' id='gender'></p>
                    </Form.Item>
                    <Checkbox name='agree' onChange={(e) => handleSetForm(e)}>I agree all statements in <a href="#">Terms of service</a>
                    <p className='text-red-500' id='agree'></p>
                    </Checkbox>
                </form>
                <p id='result' className='pt-3 text-red-500'></p>
                <button onClick={handleRegister} className='mt-5 px-5 py-3 bg-green-500 text-white font-semibold text-xl rounded-3xl'>Submit</button>
                <a className='ml-5 text-blue-500 underline' href="/login">Already have an account?</a>
            </div>
        )
    }
  return (
    <div id='register'>
        <Desktop>
            <div className='mb-10 mt-32 p-5 grid grid-cols-2 w-3/5 mx-auto shadow-lg rounded-xl'>
                <div className='left'>
                    {renderRegister()}
                </div>
                <div className='right mx-auto self-center'>
                    <img src={signup} alt="" />
                </div>
            </div>
        </Desktop>
        <Mobile>
            <div className='mb-10 mt-32 p-5 grid grid-cols-1 w-full mx-auto shadow-lg rounded-xl'>
                <div className='left'>
                    {renderRegister()}
                </div>
            </div>
        </Mobile>
        <Tablet>
            <div className='mb-10 mt-32 p-5 grid grid-cols-2 w-11/12 mx-auto shadow-lg rounded-xl'>
                <div className='left'>
                    {renderRegister()}
                </div>
                <div className='right mx-auto self-center'>
                    <img src={signup} alt="" />
                </div>
            </div>
        </Tablet>
    </div>
  )
}
