import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  groupJob: [],
  menuJob: []
}

export const jobSlice = createSlice({
  name: 'jonSlice',
  initialState,
  reducers: {
    setGroupJob: (state, action) => {
      state.groupJob = action.payload;
    },
    setMenuJob: (state,action) => {
      state.menuJob = action.payload;
    }
  },
})

// Action creators are generated for each case reducer function
export const { setGroupJob,setMenuJob } = jobSlice.actions

export default jobSlice.reducer