import axios from "axios";
import { BASE_URL, config } from "./configURL";

export const searchService = {
  getCongViecTheoTen: (name) => {
    return axios({
      url: BASE_URL + `/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${name}`,
      method: "GET",
      headers: config,
    });
  },
};
