import axios from "axios"
import { BASE_URL, config } from "./configURL"

export const userService = {
    getLogIn: (model) => {
        return axios({
            url: BASE_URL + "/api/auth/signin",
            method: "POST",
            data: model,
            headers: config
        })
    },
    getSignUp: (data) => {
        return axios({
            url: BASE_URL + "/api/auth/signup",
            method: "POST",
            data: data,
            headers: config
        })
    },
    getUpdate: (data,id) => {
        return axios({
            url: BASE_URL + "/api/users/" + id,
            method: "PUT",
            data: data,
            headers: config
        })
    },
    getUser: (id) => {
        return axios({
            url: BASE_URL + "/api/users/" + id,
            method: "GET",
            headers: config
        })
    },
    uploadAvatar: (data,token) => {
        return axios({
            url: BASE_URL + "/api/users/upload-avatar",
            method: "POST",
            data: data,
            headers: {...config,token: token}
        })
    },
    getCongViec: (token) => {
        return axios({
            url: BASE_URL + "/api/thue-cong-viec/lay-danh-sach-da-thue",
            method: "GET",
            headers: {
                ...config,token: token
            }
        })
    }
}