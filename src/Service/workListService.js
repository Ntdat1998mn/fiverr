import axios from "axios";
import { BASE_URL, config } from "./configURL";

export const workListService = {
  getGroupWork: (groupId) => {
    return axios({
      url: BASE_URL + `/api/cong-viec/lay-chi-tiet-loai-cong-viec/${groupId}`,
      method: "GET",
      headers: config,
    });
  },
  getWork: (id) => {
    return axios({
      url: BASE_URL + `/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${id}`,
      method: "GET",
      headers: config,
    });
  },
  getDetailWork: (id) => {
    return axios({
      url: BASE_URL + `/api/cong-viec/lay-cong-viec-chi-tiet/${id}`,
      method: "GET",
      headers: config,
    });
  },
  getComments: (id) => {
    return axios({
      url: BASE_URL + `/api/binh-luan/lay-binh-luan-theo-cong-viec/${id}`,
      method: "GET",
      headers: config,
    });
  },
  postComment: (model, token) => {
    return axios({
      url: BASE_URL + `/api/binh-luan`,
      method: "POST",
      data: model,
      headers: {
        ...config,
        token: token,
      },
    });
  },
  postThueCongViec: (model, token) => {
    return axios({
      url: BASE_URL + `/api/thue-cong-viec`,
      method: "POST",
      data: model,
      headers: {
        ...config,
        token: token,
      },
    });
  },
};
